from __future__ import with_statement
import os
from fabric.api import *
from fabric.contrib.console import confirm


env.project_name = os.path.dirname(os.path.abspath(__file__)).split('/')[-1]

env.project_name = 'english'

env.hosts = ['46.101.135.229']
env.deploy_user = 'root'
env.user = 'root'
env.port = '10022'
env.path = '/home/vk/languages_resources'
env.env_path = "/root/.virtualenvs/lr"
env.python_path = "/root/.virtualenvs/lr/bin/python"
env.env_root = "/root/.virtualenvs/lr"
# env.activate = 'source %s/bin/activate' %(env.code_root)

def webserver():
    "Use the actual webserver"
    pass


def virtualenv(command):
    with cd(env.code_root):
        sudo(env.activate + '&&' + command, user=env.deploy_user)


def prepare_deploy():
    local("./manage.py")


def install_requirements():
    "Install the required packages from the requirements file using pip"
    require('release', provided_by=[deploy,])
    # print run("ls")
    cd = "cd %(path)s"
    venv_command = 'source %(env_path)s/bin/activate'
    command = 'pip install  -r requirements/production.txt'
    run("; ".join([cd, venv_command, command]) % env, pty=True)


def migrate():
    require('project_name')
    require('python_path')

    cd = "cd %(path)s/src"
    venv_command = 'source %(env_path)s/bin/activate'
    command = 'python manage.py migrate --noinput'
    run("; ".join([cd, venv_command, command]) % env, pty=True)
    command = "python manage.py collectstatic --noinput"
    run("; ".join([cd, venv_command, command]) % env, pty=True)


def restart_webserver():
    run('cd %(path)s/src; touch touch_reload' % env, pty=True)


def git_update():
    require('path')
    with cd(env.path):
        run("git pull")


def deploy():
    require('hosts', provided_by=[webserver,])
    require('path')

    import time
    env.release = time.strftime('%Y%m%d%H%M%S')
    git_update()
    install_requirements()
    migrate()
    restart_webserver()




def make_dump():
    #TODO add auto backup
    # run('mysqldump -u vk -pss vk | gzip > `date +/home/vk/vk_bot/dump.sql.%Y%m%d.%H%M%S.gz`')
    # local("scp -P 10022 vk@139.59.128.154:/home/vk/21_may.sql ~/tmp")
    print("Hello %s!" % "")


def load_dump():
    local("mysql -u vk -pasdfw3eaf vk < ~/tmp/21_may.sql")
    print("done")