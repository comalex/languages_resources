from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.sitemaps.views import sitemap

from sitemap import StaticViewSitemap
import english.urls


sitemaps = {
    'static': StaticViewSitemap,
}

urlpatterns = [
    url(r'^', include(english.urls, namespace='english')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^robots\.txt', include('robots.urls')),
    url(r'^select2/', include('django_select2.urls')),

    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    # url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap')
]

# User-uploaded files like profile pics need to be served in development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)