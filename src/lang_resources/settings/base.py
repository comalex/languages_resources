# -*- coding: utf-8 -*-
"""
Django settings for lang_resources project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
import os
import sys

from django.core.urlresolvers import reverse_lazy
from os.path import dirname, join, exists

# Build paths inside the project like this: join(BASE_DIR, "directory")
BASE_DIR = dirname(dirname(dirname(__file__)))
main_folder = os.path.abspath(os.path.join(BASE_DIR, os.pardir))

STATICFILES_DIRS = [
    join(BASE_DIR, 'static'),
]

MEDIA_ROOT = join(BASE_DIR, 'media')
MEDIA_URL = "/media/"

sys.path.insert(1, main_folder)

# Use Django templates using the new Django 1.8 TEMPLATES settings
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            join(BASE_DIR, 'templates'),
            # insert more TEMPLATE_DIRS here
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',

                # 'social.apps.django_app.context_processors.backends',
                # 'social.apps.django_app.context_processors.login_redirect',
            ],
        },
    },
]

# Use 12factor inspired environment variables or from a file
import environ
env = environ.Env()

# Ideally move env file should be outside the git repo
# i.e. BASE_DIR.parent.parent
env_file = join(dirname(__file__), 'local.env')
if exists(env_file):
    environ.Env.read_env(str(env_file))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ
#
SECRET_KEY = env('SECRET_KEY')

db = env.db()
DATABASES = {
    # Raises ImproperlyConfigured exception if DATABASE_URL not in
    # os.environ
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'USER': db["USER"],
        'NAME': db["NAME"],
        'HOST': db["HOST"],
        'PASSWORD': db["PASSWORD"],
        'PORT': db["PORT"],
        # 'OPTIONS': {'init_command': 'SET storage_engine=INNODB;'}
    },
}


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',

    'sorl.thumbnail',
    'django_select2',

    'robots',

    'accounts',
    'english',

)

# AUTHENTICATION_BACKENDS = (
#     'social.backends.open_id.OpenIdAuth',
#     'social.backends.google.GoogleOpenId',
#     'social.backends.google.GoogleOAuth2',
#     'social.backends.google.GoogleOAuth',
#     'social.backends.twitter.TwitterOAuth',
#     'social.backends.yahoo.YahooOpenId',
#     'django.contrib.auth.backends.ModelBackend',
# )

MIDDLEWARE_CLASSES = (
    # 'django_hosts.middleware.HostsRequestMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django_hosts.middleware.HostsResponseMiddleware'
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

URLCONF_PATH = 'lang_resources.url_confs'

MULTI_DOMAINS = {
    "lang-resources.com": {
        "urlconf": "lang_resources.py",
        "debug_url": "english"
    },
    "postingtools.org": {
        "urlconf": "postingtools.py",
        "debug_url": "postingtools",
    }
}

ROOT_URLCONF = 'lang_resources.urls'

WSGI_APPLICATION = 'lang_resources.wsgi.application'


LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'

ALLOWED_HOSTS = []

# Crispy Form Theme - Bootstrap 3
# CRISPY_TEMPLATE_PACK = 'bootstrap3'

# For Bootstrap 3, change error alert to 'danger'
from django.contrib import messages
MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

# Authentication Settings
AUTH_USER_MODEL = 'accounts.User'
# LOGIN_REDIRECT_URL = reverse_lazy("profiles:show_self")
LOGIN_URL = "/admin/login/"

THUMBNAIL_EXTENSION = 'png'     # Or any extn for your thumbnails


# Django-cities

# Localized names will be imported for all ISO 639-1 locale codes below.
# 'und' is undetermined language data (most alternate names are missing a lang tag).
# See download.geonames.org/export/dump/iso-languagecodes.txt
# 'LANGUAGES' will match your language settings, and 'ALL' will install everything
CITIES_LOCALES = ['en', ]

# full path of folder where the data from geonames.org are downloaded
# by default all data are downloaded in folder 'data' relative to package installation
CITIES_DATA_DIR = '/var/tmp/'

# Postal codes will be imported for all ISO 3166-1 alpha-2 country codes below.
# You can also specificy 'ALL' to import all postal codes.
# See cities.conf for a full list of country codes. 'ALL' will install everything.
# See download.geonames.org/export/dump/countryInfo.txt
CITIES_POSTAL_CODES = ['US', ]

# List of plugins to process data during import
CITIES_PLUGINS = [
    'cities.plugin.reset_queries.Plugin',  # plugin that helps to reduce memory usage when importing large datasets (e.g. "allCountries.zip")
]

SITE_ID = 2

INTERNAL_IPS = (
    '127.0.0.1',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
    }
}
