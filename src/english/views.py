# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView
from django.http import JsonResponse

from .forms import SiteForm
from .models import Site, Category, Tag


class Index(TemplateView):
    template_name = "english/index.html"

    # @method_decorator(cache_page(60*60*5))
    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        ctx["sites"] = Site.objects.all()
        ctx["tags"] = Tag.objects.all()
        ctx["form"] = SiteForm()
        return self.render_to_response(ctx)


def add_site(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SiteForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            return JsonResponse({"status": True})
    else:
        form = SiteForm()
    return render(request, "english/site_add_form.html", {"form": form})


def select2(request):
    tags = Tag.objects.all()
    results = [{'id': index, 'text': value} for (index, value) in enumerate(tags)]
    return JsonResponse({'err': 'nil', 'results': results})