# -*- coding: utf-8 -*-
from django.contrib import admin
from django_select2.forms import ModelSelect2TagWidget
from .models import Site, PricePolicy, Languages, Tag
from django import forms

from sorl.thumbnail.admin import AdminImageMixin



class PricePolicyInline(admin.StackedInline):
    model = PricePolicy
    extra = 1


class CustomWidget(ModelSelect2TagWidget):
    model = Tag
    search_fields = [
        'name__startswith',
        'pk__startswith'
    ]

    def build_attrs(self, extra_attrs=None, **kwargs):
        attrs = super(CustomWidget, self).build_attrs(extra_attrs=extra_attrs, **kwargs)
        attrs['data-token-separators'] = ','
        return attrs

    def value_from_datadict(self, data, files, name):
        values = super(CustomWidget, self).value_from_datadict(data, files, name)
        cleaned_values = []
        for value in values:
            if not value.isdigit():
                obj_id = self.model.objects.create(name=value).pk
            else:
                obj_id = int(value)
            cleaned_values.append(obj_id)
        return cleaned_values


class ContentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContentForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Site
        exclude = ()
        widgets = {
            'tags': CustomWidget,
        }

    def clean_tags(self, *args, **kwargs):
        return self.cleaned_data["tags"]


def remove_images(modeladmin, request, queryset):
    for site in queryset:
        site.image.delete()

remove_images.short_description = "Delete all images"


@admin.register(Site)
class SiteAdmin(AdminImageMixin, admin.ModelAdmin):
    form = ContentForm
    filter_horizontal = ('tags',)
    exclude = ("categories", )
    radio_fields = {"price_policy": admin.VERTICAL, 'rating': admin.HORIZONTAL}

    list_display = ('link', 'sort_description', 'moderated', 'show_preview', )
    list_filter = ('languages', 'link', 'categories')
    ordering = ('moderated', )
    search_fields = ('link',)
    actions = [remove_images]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ("name", "slug")


admin.site.register(PricePolicy)
admin.site.register(Languages)
