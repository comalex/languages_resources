# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-08 07:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('english', '0002_auto_20160808_0116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='site',
            name='categories',
            field=models.ManyToManyField(blank=True, related_name='site', to='english.Category'),
        ),
        migrations.AlterField(
            model_name='site',
            name='link',
            field=models.URLField(help_text='Example: https://example.com', max_length=500),
        ),
    ]
