# -*- coding: utf-8 -*-
import os
import logging
from urlparse import urlparse
from django.core.management import BaseCommand
from django.core.files.base import ContentFile

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
from requests import get

from english.models import Site


logger = logging.getLogger("english")


class Command(BaseCommand):
    help = 'Make site screenshot'

    def add_arguments(self, parser):

        parser.add_argument("-u", '--user-agent', action='store',
                          dest="user_agent", type=str,
                          default="Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML,\
                              like Gecko) Chrome/41.0.2228.0 Safari/537.36",
                          help='The user agent used for requests')
        parser.add_argument("-t", '--timeout', action='store',
                          dest="timeout", type=int, default=10,
                          help='Number of seconds to try to resolve')



    def handle(self, *args, **options):
        hosts = []
        timeout = options.get("timeout", 100)
        user_agent = options.get("user_agent", "Mozilla/5.0 (Windows NT\
                6.1) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/41.0.2228.\
                0 Safari/537.36")



        for site in Site.objects.filter(image=""):
            dcap = dict(DesiredCapabilities.PHANTOMJS)
            dcap["phantomjs.page.settings.userAgent"] = user_agent
            dcap["accept_untrusted_certs"] = True
            driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true'], desired_capabilities=dcap) # or add to your PATH
            driver.set_window_size(1024, 768) # optional
            driver.set_page_load_timeout(timeout)
            try:
                res = self.capture_snaps([site.link], driver, os.getcwd(), timeout, True)

                for host, value in res.items():
                    if value["path"]:
                        with open(os.path.join(os.getcwd(), value["path"])) as f:
                            data = f.read()
                            site.image.save('%s.png' % value["image_name"], ContentFile(data))
            except Exception as e:
                print e
            try:
                driver.close()
            except Exception as e:
                print e

            try:
                driver.quit()
            except Exception as e:
                print e

    def save_image(self, uri, file_name, driver):
        try:
            driver.get(uri)
            driver.save_screenshot(file_name)
            return True
        except TimeoutException:
            return False

    def host_reachable(self, host, timeout):
        try:
            get(host, timeout=timeout, verify=False)
            return True
        except:
            return False

    def host_worker(self, host, driver, timeout, verbose):

        res = ""
        if not host.startswith("http://") and not host.startswith("https://"):
            host = "http://" + host

        image_name = urlparse(host).netloc
        image_name = image_name if len(image_name) > 5 else host
        filename = os.path.join("output", "images", image_name + ".png")
        if verbose:
            print("Fetching %s" % host)
        if not self.host_reachable(host, timeout) and "https" not in host:
            host = host.replace("http", "https")
        if self.host_reachable(host, timeout) and self.save_image(host, filename, driver):
            res = filename
        else:
            if verbose:
                print("%s is unreachable or timed out" % host)

        return {
            "path": res,
            "image_name": image_name
        }


    def capture_snaps(self, hosts, driver, outpath, timeout=10, verbose=True):
        outpath = os.path.join(outpath, "output")
        imagesOutputPath = os.path.join(outpath, "images")
        if not os.path.exists(outpath):
            os.makedirs(outpath)
        if not os.path.exists(imagesOutputPath):
            os.makedirs(imagesOutputPath)

        results = {}
        for host in hosts:
            results[host] = self.host_worker(host, driver, timeout, verbose)

        return results


