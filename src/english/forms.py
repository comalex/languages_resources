# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.contrib.auth import get_user_model


from .models import Site

User = get_user_model()


class SiteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SiteForm, self).__init__(*args, **kwargs)
        self.fields['price_policy'].empty_label = None

    class Meta:
        model = Site
        exclude = ("languages", "rating")
        widgets = {
            'price_policy': forms.RadioSelect,
        }
