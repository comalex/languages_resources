# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.utils.text import slugify
from sorl.thumbnail import ImageField, get_thumbnail
from transliterate import translit


@python_2_unicode_compatible
class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(verbose_name='Slug', unique=True, max_length=100)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Languages(models.Model):
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class PricePolicy(models.Model):
    text = models.CharField(max_length=100)
    help = models.TextField(max_length=500, blank=True, null=True)
    css_class = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.text


@python_2_unicode_compatible
class Tag(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(verbose_name='Slug', unique=True, max_length=100, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.title()
        self.slug = slugify(translit(self.name, 'ru', reversed=True))
        super(Tag, self).save(*args, **kwargs)


@python_2_unicode_compatible
class Site(models.Model):
    link = models.URLField(max_length=500, help_text="Example: https://example.com")
    name = models.CharField(max_length=500, blank=True)
    description = models.TextField(max_length=500)
    tags = models.ManyToManyField(Tag, blank=True, related_name='sites')
    price_policy = models.ForeignKey(PricePolicy)
    is_recommend = models.BooleanField("Выбор редакции", default=False)
    image = ImageField(upload_to="english/site_images", blank=True, null=True)
    categories = models.ManyToManyField(Category, related_name="site", blank=True)
    languages = models.ManyToManyField(Languages, related_name="site")
    rating = models.IntegerField(choices=[(i, i) for i in range(1, 6)], default=5)
    moderated = models.BooleanField("Модерация", default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('rating', )

    def sort_description(self):
        return self.description[:50] + ".."

    def show_preview(self):
        if self.image:
            im = get_thumbnail(self.image, '60x60', crop='center', quality=99)
            return '<img src="%s" />' % im.url

    show_preview.allow_tags = True
    show_preview.short_description = 'Image preview'