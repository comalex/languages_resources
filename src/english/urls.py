# -*- coding:utf-8 -*-
from django.conf.urls import url
from .views import Index, add_site, select2

urlpatterns = [
    url(r'^$', Index.as_view(), name='index'),
    url(r'^select2/$', select2, name='select2'),
    url(r'^site/add/$', add_site, name='site_add'),
]