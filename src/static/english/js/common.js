function wResize() {
    var window_width = $(window).width();
    if (window_width < 992) {
        $('#aside-menu').addClass('fixed');
        $('#aside-menu').addClass('aside-hide');

    } else if (window_width > 992) {
        $("#aside-menu").removeClass('aside-hide');
        $("#aside-menu").removeClass('fixed');
    }   

    var $menu_button = $(".filtre-btn");
    var $menu_button_2 = $("#close-menu");

    $menu_button.click(function(e) {
        $('#aside-menu').removeClass('aside-hide');
        $(".filtre-btn").addClass('open-menu');
    });

    $menu_button_2.click(function(e) {
        $('#aside-menu').addClass('aside-hide');
        $(".filtre-btn").removeClass('open-menu');
    });

};

$(document).load(function() {
    wResize();
    $(window).resize(function() {
        wResize();
    });
});

$(document).ready(function() {
    wResize();
    $(window).resize(function() {
        wResize();
    });

    $('.popup').magnificPopup({
             removalDelay: 300,
             mainClass: 'mfp-fade'
    });

    $("body").on("submit", "#offer-form", function(e) {
        e.preventDefault();
        var $form = $(this);
        $.post($form.attr("action"), $(this).serialize(), function(response) {
            if (response.status) {
            } else {
                $form.replaceWith(response);
            }
        });
    });
});


$(function() {
    var StickyElement = function(node){
      var doc = $(document), 
          fixed = false,
          anchor = node.find('.sticky-anchor'),
          content = node.find('.menu-wrap');
      
      var onScroll = function(e){
        var docTop = doc.scrollTop(),
            anchorTop = anchor.offset().top;
        
        //console.log('scroll', docTop, anchorTop);
        if(docTop > anchorTop){
          if(!fixed){
            anchor.height(content.outerHeight());
            content.addClass('fixed');              
            fixed = true;
          }
        }  else   {
          if(fixed){
            anchor.height(0);
            content.removeClass('fixed'); 
            fixed = false;
          }
        }
      };
      
      $(window).on('scroll', onScroll);
    };

    var demo = new StickyElement($('aside'));
});

$(function() {


    var StickyElement2 = function(node){
      var doc = $(document), 
          fixed = false,
          anchor = node.find('.sticky-anchor2'),
          menu_btn = node.find('.filtre-btn');
      
      var onScroll = function(e){
        var docTop = doc.scrollTop(),
            anchorTop = anchor.offset().top;
        
        //console.log('scroll', docTop, anchorTop);
        if(docTop > anchorTop){
          if(!fixed){
            anchor.height(menu_btn.outerHeight());      
            menu_btn.addClass('filtre-btn-fixed');        
            fixed = true;
          }
        }  else   {
          if(fixed){
            anchor.height(0);
            menu_btn.removeClass('filtre-btn-fixed'); 
            fixed = false;
          }
        }
      };
      
      $(window).on('scroll', onScroll);
    };

    var demo = new StickyElement2($('.button'));



    $(".skills .toggle").click(function() {
        var main_category = $(this).attr("data-slug");
        if (main_category === "all") {
            if ($(".skills .items input:checked").length == 0) {
                $(".toggle.main input").prop('checked', true);
            }
            $(".site-wrap").show();
            $(".skills .items input").prop('checked', false);
        } else {
            $(".toggle.main input").prop('checked', false);

            var show_categories = [];
            $(".skills .items input:checked").each(function() {
                var category = $(this).parent().attr("data-slug");
                if (category) {
                    show_categories.push(category);
                }
            });

            if (show_categories.length == 0) {
                $(".toggle.main input").prop('checked', true);
                $(".site-wrap").show();
                return;
            }
            $(".site-wrap").each(function () {
                var categories = $(this).attr("data-categories").split(" ");
                var $site = $(this);
                var show = false;
                $.each(show_categories, function(i, category) {
                    if (jQuery.inArray(category, categories) != -1) {
                        show = true;
                    }
                });

                if (show) {
                    $site.show();
                } else{
                    $site.hide();
                }
            });
        }
    });

});


